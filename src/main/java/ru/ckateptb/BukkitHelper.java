package ru.ckateptb;

import com.gamerforea.eventhelper.util.EventUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;

public class BukkitHelper {
    private static boolean isTableclothServer = false;

    static {
        try {
            Class.forName("tablecloth.server.TableclothServer");
            isTableclothServer = true;
        } catch (ClassNotFoundException e) {
        }
    }

    public static boolean canAttack(EntityPlayer player) {
        if (!isTableclothServer)
            return true;
        return !EventUtils.cantInteract(player, EnumHand.MAIN_HAND, player.getPosition(), EnumFacing.UP); //TODO Debug cantAttack self
    }
}
